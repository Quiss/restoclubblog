<?php

namespace RestoClub\BlogBundle\Controller;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use RestoClub\BlogBundle\Entity\Post;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;

/** Annotations configurator */
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends Controller
{
    /**
     * @Route("/{tagId}", name="blog.list.posts", requirements={"tagId" = "\d+"}, defaults={"tagId" = null})
     * @Template("RestoClubBlogBundle:Default:list.html.twig")
     * @Method({"GET"})
     * @param null $tagId
     * @return array
     */
    public function getListPosts($tagId = null)
    {
        $posts = $this->getDoctrine()->getRepository('RestoClubBlogBundle:Post');

        if (is_null($tagId)) {
            $posts = $posts->findAll();
        } else {
            $posts = $posts->findAllByTags($tagId);
        }

        $tags = $this->getDoctrine()->getRepository('RestoClubBlogBundle:Tags')->findAll();

        return [
            'posts' => $posts,
            'availableTags' => $tags
        ];
    }

    /**
     * @Route("/create", name="blog.create.post")
     * @Route("/edit/{id}", name="blog.edit.post", requirements={"id" = "\d+"})
     * @Method({"GET","POST"})
     * @Template("RestoClubBlogBundle:Form:create.html.twig")
     * @param Request $request
     * @param null $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createPostAction(Request $request, $id = null)
    {
        $type = $request->get('type');
        if (is_null($id)) {
            $postEntity = new Post();
        } else {
            $postEntity = $this->getDoctrine()->getRepository('RestoClubBlogBundle:Post')->find($id);

            if (!$postEntity) {
                throw $this->createNotFoundException('[BB-040916] Id to edit not found!');
            }
        }

        $validationGroups = [
            'Default'
        ];

        if ($type == 'author' || (!is_null($id) && $postEntity->getType() == 2)) {
            $validationGroups[] = 'author_type';
        }

        $formBuilder = $this->createFormBuilder($postEntity, [
            'validation_groups' => $validationGroups,
        ]);

        $formBuilder
            ->add('title', null, [
                'label' => 'Заголовок',
                'required' => true,
            ])
            ->add('body', TextareaType::class, [
                'label' => 'Текст сообщения',
                'attr' => [],
                'required' => true,
            ]);

        if ($type == 'author' || (!is_null($id) && $postEntity->getType() == 2)) {
            $formBuilder
                ->add('author', null, [
                    'label' => 'Автор',
                    'required' => true,
                ])
                ->add('link', null, [
                    'label' => 'Ссылка на авторскую статью',
                    'required' => true,
                ]);
        }

        $formBuilder
            ->add('tags', EntityType::class, array(
                'class' => 'RestoClubBlogBundle:Tags',
                'label' => 'Доступные тэги',
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true
            ));

        $formPrepared = $formBuilder->getForm();

        $formPrepared->handleRequest($request);

        if ($formPrepared->isSubmitted() && $formPrepared->isValid()) {
            $postEntity = $formPrepared->getData();
            $em = $this->getDoctrine()->getManager();
            if ($type == 'author') {
                $postEntity->setType(2);
            }
            $em->persist($postEntity);
            $em->flush();

            return $this->redirect($this->generateUrl('blog.list.posts'));
        }

        return [
            'create_post_form' => $formPrepared->createView()
        ];
    }

    /**
     * @Route("/show/{id}", requirements={"id" = "\d+"}, name="blog.show.post")
     * @Template("RestoClubBlogBundle:Default:show.html.twig")
     * @Method({"GET"})
     */
    public function showAction($id)
    {
        $post = $this->getDoctrine()->getRepository('RestoClubBlogBundle:Post')->find($id);

        if (!$post) {
            throw $this->createNotFoundException('[BB-040916] Post not found!');
        }

        return [
            'post' => $post
        ];
    }
}
