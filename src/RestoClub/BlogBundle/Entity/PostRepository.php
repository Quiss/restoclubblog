<?php
namespace RestoClub\BlogBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PostRepository extends EntityRepository
{
    public function findAllByTags($tagId = null)
    {
        return $this->createQueryBuilder("p")
            ->join("p.tags", "t", "WITH", "t.id=:tagId")
            ->setParameter("tagId", $tagId)->addSelect("t")
            ->getQuery()->getResult();
    }
}